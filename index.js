	

	/*1.  Create a function which will be able to add two numbers.
		-Numbers must be provided as arguments.
		-Display the result of the addition in our console.
		-function should only display result. It should not return anything.*/

		function sum(valueOne, valueTwo){
			let addition = valueOne + valueTwo;
			console.log('Displayed the sum of ' + valueOne + ' and ' + valueTwo);
			console.log(addition);
		};
		sum(5,15);

		/*Create a function which will be able to subtract two numbers.
		-Numbers must be provided as arguments.
		-Display the result of subtraction in our console.
		-function should only display result. It should not return anything.*/

		function difference(valueOne, valueTwo){
			let addition = valueOne - valueTwo;
			console.log('Displayed the difference of ' + valueOne + ' and ' + valueTwo);
			console.log(addition);
		};
		difference(20,5);

		/*-invoke and pass 2 arguments to the addition function
		-invoke and pass 2 arguments to the subtraction function*/

	/*2.  Create a function which will be able to multiply two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the multiplication.*/

		function product(valueOne, valueTwo){
			let addition = valueOne * valueTwo;
			console.log('Displayed the product of ' + valueOne + ' and ' + valueTwo);
			console.log(addition);
		};
		product(50,10);


		/*Create a function which will be able to divide two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the division.*/

		function quotient(valueOne, valueTwo){
			let addition = valueOne / valueTwo;
			console.log('Displayed the quotient of ' + valueOne + ' and ' + valueTwo);
			console.log(addition);
		};
		quotient(50,10);

		/*Create a global variable called outside of the function called quotient.
			-This quotient variable should be able to receive and store the result of division function*/

		/*Log the value of product variable in the console.
		Log the value of quotient variable in the console.*/

	/*3. 	Create a function which will be able to get total area of a circle from a 			provided radius.
			-a number should be provided as an argument.
			-look up the formula for calculating the area of a circle with a provided/given radius.
			-look up the use of the exponent operator.
			-you can save the value of the calculation in a variable.
			-return the result of the area calculation.*/

		function radius(value){
			let pi =  3.1416;
			let area = pi * value * value;
			console.log('The result of getting the area of a circle with ' + value + ' radius:');
			console.log(area);
		};
		radius(15);

		/*Create a global variable called outside of the function called circleArea.
			-This variable should be able to receive and store the result of the circle area calculation.*/

	/*Log the value of the circleArea variable in the console.*/

	/*4. 	Create a function which will be able to get total average of four numbers.
			-4 numbers should be provided as an argument.
			-look up the formula for calculating the average of numbers.
			-you can save the value of the calculation in a variable.
			-return the result of the average calculation.*/

		function average(valueOne, valueTwo, valueThree, valueFour){
			let numbers = valueOne + valueTwo + valueThree + valueFour;
			let answer = numbers / 4;
			console.log('The average of ' + valueOne + ',' + valueTwo + ',' + valueThree + ' and ' +valueFour);
			console.log(answer);
		};
		average(20,40,60,80);

	    /*Create a global variable called outside of the function called averageVar.
			-This variable should be able to receive and store the result of the average calculation
			-Log the value of the averageVar variable in the console.*/
	

	/*5. Create a function which will be able to check if you passed by checking the percentage of your score against the passing percentage.
			-this function should take 2 numbers as an argument, your score and the total score.
			-First, get the percentage of your score against the total. You can look up the formula to get percentage.
			-Using a relational operator, check if your score percentage is greater than 75, the passing percentage. Save the value of the comparison in a variable called isPassed.
			-return the value of the variable isPassed.
			-This function should return a boolean.*/

		function passingScore(valueOne, valueTwo){
			let percentage = valueTwo * 0.75;
			let answer = valueOne > percentage;
			console.log('Is ' + valueOne + '/' + valueTwo + ' a passing score?');
			console.log(answer);
		};
		passingScore(38,50);

		/*Create a global variable called outside of the function called isPassingScore.
			-This variable should be able to receive and store the boolean result of the checker function.
			-Log the value of the isPassingScore variable in the console.*/
